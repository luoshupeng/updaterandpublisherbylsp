﻿#region Class Header
/* ========================================================================
* 【本类功能概述】
* 
* 作者：chinesedragon 
* 时间：5/4/2013 3:53:41 PM
* 文件名：FormCheckNew
* 版本：V1.0.1
*
* 修改时间： 
* 修改说明：
* ========================================================================
*/
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Updater
{
    public partial class FormCheckNew : FormBase
    {
        private readonly BackgroundWorker worker = null;
        private ReleaseList localReleaseList = null;
        private ReleaseList remoteReleaseList = null;

        public FormCheckNew()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_WorkComplete;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                int CS_NOCLOSE = 0x0200;
                CreateParams parameters = base.CreateParams;
                parameters.ClassStyle |= CS_NOCLOSE;
                return parameters;
            }
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //下载远程ReleaseList到临时文件夹
                string localPath = Path.GetTempPath();
                string remotePath = localReleaseList.ReleaseUrl;
                string user = localReleaseList.UserName;
                string pwd = localReleaseList.UserPassword;
                long downSize = 0;
                using (DownloadHelper downloader = DownloadHelperFactory.CreateInstance(remotePath, user, pwd))
                {
                    downloader.DownloadFile(localPath, remotePath, GlobalValue.ReleaseListName, ref downSize);
                }
                //加载临时文件夹中的远程ReleaseList
                if (downSize > 0)
                {
                    remoteReleaseList = XmlSerializeHelper.LoadObjectFromXml<ReleaseList>(localPath + "\\" + GlobalValue.ReleaseListName);
                    e.Result = remoteReleaseList;
                }
                else
                {
                    throw new Exception("Download " + GlobalValue.ReleaseListName + " Error!");
                }
            }
            catch (System.Exception ex)
            {
                e.Result = ex.Message;
                throw;
            }
        }

        private void worker_WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("获取不到升级文件，现在启动老版本程序\n"+e.Error.Message);
                GlobalValue.StartExec(localReleaseList.StartupExec, "检查更新过程出错！");
                Application.Exit();
                return;
            }
            //比较本地和远程的版本
            if (GlobalValue.CompareVersion(localReleaseList.ReleaseVersion, remoteReleaseList.ReleaseVersion) == 0)
            {//没有新版本
                GlobalValue.StartExec(localReleaseList.StartupExec);
                Application.Exit();
                return;
            }
            
            //显示更新窗口
            this.Hide();
            FormUpgradeNew formUpgrade = new FormUpgradeNew(localReleaseList, remoteReleaseList);
            formUpgrade.ShowDialog(this);
        }

        private void FormCheckNew_Load(object sender, EventArgs e)
        {
            //初始化本地ReleaseList
            string localReleaseListPath = Path.Combine(Application.StartupPath + "\\" + GlobalValue.ReleaseListName);
            localReleaseList = XmlSerializeHelper.LoadObjectFromXml<ReleaseList>(localReleaseListPath);
            if (localReleaseList == null)
            {
                MessageBox.Show("本地缺少" + GlobalValue.ReleaseListName + "文档，无法启动程序！");
                this.Close();
            }
            //获取远程ReleaseList
            worker.RunWorkerAsync();            
        }
    }
}
