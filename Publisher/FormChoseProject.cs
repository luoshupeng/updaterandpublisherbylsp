﻿#region Class Header
/* ========================================================================
* 【本类功能概述】
* 
* 作者：chinesedragon 
* 时间：4/26/2013 10:34:41 AM
* 文件名：FormChoseProject
* 版本：V1.0.1
*
* 修改时间： 
* 修改说明：
* ========================================================================
*/
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TooltipListBox;

namespace Publisher
{
    public partial class FormChoseProject : FormBase
    {
        private HistoryList historyList = null;
        private string projectType;
        private string historyPath;
        private ProjectInfoClass proInfo;

        public FormChoseProject()
        {
            InitializeComponent();
            projectType = "C#工程文件";            
        }

        #region CreateParams        
        /// <summary>
        /// 去除最大化和最小化按钮
        /// 改窗口边框为不可伸缩
        /// </summary>
        protected override CreateParams CreateParams
        {            
            get
            {
                int WS_MINIMIZEBOX = 0x00020000;
                int WS_MAXIMIZEBOX = 0x00010000;
                int WS_BORDER = 0x00800000;
                int WS_THICKFRAME = 0x00040000;
                CreateParams param = base.CreateParams;
                param.Style &= ~WS_MAXIMIZEBOX;
                param.Style ^= WS_MINIMIZEBOX;
                param.Style ^= WS_THICKFRAME;
                param.Style |= WS_BORDER;
                return param;
            }
        }
        #endregion

        private void FormChoseProject_Load(object sender, EventArgs e)
        {
            historyPath = GlobalValue.LogDataPath + GlobalValue.HistoryListName;            
            
            historyList = XmlSerializeHelper.LoadObjectFromXml<HistoryList>(historyPath);
            if (historyList == null)
            {
                historyList = new HistoryList();
            }

            List<ListBoxItem> items = new List<ListBoxItem>();
            foreach (HistoryItem item in historyList.HistoryItems)
            {
                items.Add(new ListBoxItem(item.ProjectName, item.ProjectFullPath + "\t" + item.ProjectType));
            }

            listHistory.Items.AddRange(items.ToArray());
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (projectType == "C#工程文件")
            {
                openFileDialog.Filter = "C#工程文件(*.csproj)|*.csproj";
            }
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxLocation.Text = openFileDialog.FileName;
            }
        }

        private void radio_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radio = sender as RadioButton;
            if (radio.Tag.ToString().ToLower() == "csproj")
            {
                openFileDialog.Filter = "C#工程文件(*.csproj)|*.csproj";
                projectType = "C#工程文件";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxLocation.Text.Trim()))
            {
                MessageBox.Show("亲，您还没有选择工程哦！");
                return;
            }
            if (!File.Exists(textBoxLocation.Text.Trim()))
            {
                string pathdummy = textBoxLocation.Text;
                string namedummy = Path.GetFileNameWithoutExtension(pathdummy);
                string msg = namedummy + "更换了位置或者已经删除了，是否将它从最近列表中删除？";
                if (MessageBox.Show(msg,"",MessageBoxButtons.YesNo,MessageBoxIcon.Stop)
                    == DialogResult.Yes)
                {
                    if (listHistory.SelectedIndex != -1)
                    {
                        int nIndex = listHistory.SelectedIndex;
                        listHistory.Items.RemoveAt(nIndex);
                        textBoxLocation.Text = string.Empty;
                        historyList.HistoryItems.RemoveAt(nIndex);
                        XmlSerializeHelper.SaveXmlFormObject(historyPath, historyList);
                    }
                }
                return;
            }
            //将这一次的浏览加入到历史记录中
            string path = textBoxLocation.Text;
            string name = Path.GetFileNameWithoutExtension(path);
            historyList.InsertItem(new HistoryItem
            {
                ProjectName = name,
                ProjectFullPath = path,
                ProjectType = projectType,
            });

            //反序列化ProjectInfo
            string proPath = GlobalValue.MD5(textBoxLocation.Text);
            proPath = Path.Combine(GlobalValue.ProjectDataPath, proPath);
            proInfo = XmlSerializeHelper.LoadObjectFromXml<ProjectInfoClass>(proPath + "\\" + GlobalValue.ProjectInfoName);
            if ( proInfo == null)
            {
                proInfo = new ProjectInfoClass();
                proInfo.pName = historyList.HistoryItems[0].ProjectName;
                proInfo.pFullPath = historyList.HistoryItems[0].ProjectFullPath;
                proInfo.pType = historyList.HistoryItems[0].ProjectType;
                proInfo.pVersion = "1.0.0.0";
            }            

            XmlSerializeHelper.SaveXmlFormObject(historyPath, historyList);

            FormProjectInfo formPro = new FormProjectInfo(proInfo);
            this.Hide();
            formPro.ShowDialog(this);
            this.Close();                       
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listHistory_DoubleClick(object sender, EventArgs e)
        {
            int nIndex = listHistory.SelectedIndex;
            if (nIndex == ListBox.NoMatches)
                return;
            ListBoxItem item = (ListBoxItem)listHistory.SelectedItem;
            string name = item.DisplayText;
            string[] dummys = item.TooltipText.Split('\t');
            string path = dummys[0];
            projectType = dummys[1];
            textBoxLocation.Text = path;
            //执行下一步
            btnNext_Click(btnNext, new EventArgs());
        }

        private void FormChoseProject_Shown(object sender, EventArgs e)
        {
            string msg = "亲爱的程序员，我在此郑重向你提示:\n你的程序发布之前一定要处于Release状态！！！";
            MessageBox.Show(msg, "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
