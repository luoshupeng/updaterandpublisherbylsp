﻿#region Class Header
/* ========================================================================
* 【本类功能概述】
* 
* 作者：chinesedragon 
* 时间：4/27/2013 10:37:42 AM
* 文件名：FormProjectInfo
* 版本：V1.0.1
*
* 修改时间： 
* 修改说明：
* ========================================================================
*/
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Publisher
{
    public partial class FormProjectInfo : FormBase
    {
        private ProjectInfoClass proInfo;

        public FormProjectInfo(ProjectInfoClass pInfo)
        {
            InitializeComponent();
            proInfo = pInfo;
        }

        private void FormProjectInfo_Load(object sender, EventArgs e)
        {
            BindProjectInfo();
        }

        private void BindProjectInfo()
        {
            dataGridViewProInfo.Rows.Clear();
            //分析工程,获取工程的路径、可执行程序等
            ProjectAnalyzeHelper.AnalyzeProject(proInfo.pFullPath, proInfo.pType, ref proInfo);
            dataGridViewProInfo.Rows.Add(proInfo.pName, proInfo.pVersion, proInfo.pType, proInfo.pFullPath);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (dataGridViewProInfo.Rows[0].Cells[1].Value == null)
            {
                MessageBox.Show("d(╯﹏╰)b，好歹您也给个版本号啊！");
                return;
            }
            string newVersion = dataGridViewProInfo.Rows[0].Cells[1].Value.ToString();
            if (GlobalValue.CompareVersion(newVersion,proInfo.pVersion) != 0)
            {
                proInfo.pVersion = newVersion;
                //保存新版本
                ProjectAnalyzeHelper.AnalyzeProject(proInfo.pFullPath, proInfo.pType, ref proInfo, true);
            }            

            this.Hide();
            FormReleaseFiles formFiles = new FormReleaseFiles(proInfo);
            formFiles.ShowDialog(this);
        }
    }
}
