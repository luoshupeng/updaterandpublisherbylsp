﻿namespace Publisher
{
    partial class FormRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSize = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.labelUser = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radioW32share = new System.Windows.Forms.RadioButton();
            this.radioFtp = new System.Windows.Forms.RadioButton();
            this.radioHttp = new System.Windows.Forms.RadioButton();
            this.radioLocalFolder = new System.Windows.Forms.RadioButton();
            this.radioNone = new System.Windows.Forms.RadioButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Controls.Add(this.btnCancel);
            this.panel3.Controls.Add(this.btnNext);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 247);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(547, 36);
            this.panel3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Publisher By Lsp";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(3, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(345, 2);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(441, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Location = new System.Drawing.Point(360, 6);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "确定(&O)";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.labelSize);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.labelPassword);
            this.panel1.Controls.Add(this.txtUserName);
            this.panel1.Controls.Add(this.labelUser);
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.txtLocation);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.radioW32share);
            this.panel1.Controls.Add(this.radioFtp);
            this.panel1.Controls.Add(this.radioHttp);
            this.panel1.Controls.Add(this.radioLocalFolder);
            this.panel1.Controls.Add(this.radioNone);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 237);
            this.panel1.TabIndex = 3;
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.BackColor = System.Drawing.SystemColors.Control;
            this.labelSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelSize.Location = new System.Drawing.Point(237, 211);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(0, 12);
            this.labelSize.TabIndex = 13;
            this.labelSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 204);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(534, 23);
            this.progressBar1.TabIndex = 12;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(321, 157);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(114, 21);
            this.txtPassword.TabIndex = 11;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(262, 160);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 12);
            this.labelPassword.TabIndex = 10;
            this.labelPassword.Text = "用户密码";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(129, 157);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(114, 21);
            this.txtUserName.TabIndex = 9;
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(70, 160);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(53, 12);
            this.labelUser.TabIndex = 8;
            this.labelUser.Text = "登录用户";
            // 
            // btnBrowse
            // 
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.Location = new System.Drawing.Point(441, 123);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(59, 23);
            this.btnBrowse.TabIndex = 7;
            this.btnBrowse.Text = "浏览(&B)";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(72, 123);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(363, 21);
            this.txtLocation.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "发布网址";
            // 
            // radioW32share
            // 
            this.radioW32share.AutoSize = true;
            this.radioW32share.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioW32share.Location = new System.Drawing.Point(15, 73);
            this.radioW32share.Name = "radioW32share";
            this.radioW32share.Size = new System.Drawing.Size(88, 16);
            this.radioW32share.TabIndex = 4;
            this.radioW32share.TabStop = true;
            this.radioW32share.Tag = "W32SHARE";
            this.radioW32share.Text = "Windows共享";
            this.radioW32share.UseVisualStyleBackColor = true;
            this.radioW32share.Click += new System.EventHandler(this.RadioButton_CheckeChanged);
            // 
            // radioFtp
            // 
            this.radioFtp.AutoSize = true;
            this.radioFtp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioFtp.Location = new System.Drawing.Point(15, 51);
            this.radioFtp.Name = "radioFtp";
            this.radioFtp.Size = new System.Drawing.Size(40, 16);
            this.radioFtp.TabIndex = 3;
            this.radioFtp.TabStop = true;
            this.radioFtp.Tag = "FTP";
            this.radioFtp.Text = "FTP";
            this.radioFtp.UseVisualStyleBackColor = true;
            this.radioFtp.Click += new System.EventHandler(this.RadioButton_CheckeChanged);
            // 
            // radioHttp
            // 
            this.radioHttp.AutoSize = true;
            this.radioHttp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioHttp.Location = new System.Drawing.Point(15, 95);
            this.radioHttp.Name = "radioHttp";
            this.radioHttp.Size = new System.Drawing.Size(46, 16);
            this.radioHttp.TabIndex = 2;
            this.radioHttp.TabStop = true;
            this.radioHttp.Tag = "HTTP";
            this.radioHttp.Text = "HTTP";
            this.radioHttp.UseVisualStyleBackColor = true;
            this.radioHttp.Visible = false;
            this.radioHttp.Click += new System.EventHandler(this.RadioButton_CheckeChanged);
            // 
            // radioLocalFolder
            // 
            this.radioLocalFolder.AutoSize = true;
            this.radioLocalFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioLocalFolder.Location = new System.Drawing.Point(15, 30);
            this.radioLocalFolder.Name = "radioLocalFolder";
            this.radioLocalFolder.Size = new System.Drawing.Size(82, 16);
            this.radioLocalFolder.TabIndex = 1;
            this.radioLocalFolder.TabStop = true;
            this.radioLocalFolder.Tag = "LOCAL";
            this.radioLocalFolder.Text = "本地文件夹";
            this.radioLocalFolder.UseVisualStyleBackColor = true;
            this.radioLocalFolder.Click += new System.EventHandler(this.RadioButton_CheckeChanged);
            // 
            // radioNone
            // 
            this.radioNone.AutoSize = true;
            this.radioNone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioNone.Location = new System.Drawing.Point(15, 8);
            this.radioNone.Name = "radioNone";
            this.radioNone.Size = new System.Drawing.Size(58, 16);
            this.radioNone.TabIndex = 0;
            this.radioNone.TabStop = true;
            this.radioNone.Tag = "NONE";
            this.radioNone.Text = "不发布";
            this.radioNone.UseVisualStyleBackColor = true;
            this.radioNone.Click += new System.EventHandler(this.RadioButton_CheckeChanged);
            // 
            // FormRelease
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(567, 286);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRelease";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "发布方式";
            this.Load += new System.EventHandler(this.FormRelease_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioNone;
        private System.Windows.Forms.RadioButton radioLocalFolder;
        private System.Windows.Forms.RadioButton radioFtp;
        private System.Windows.Forms.RadioButton radioHttp;
        private System.Windows.Forms.RadioButton radioW32share;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}