﻿using System;

public class ProjectInfoClass
{
    public string pVersion { get; set; }
    public string pDescription { get; set; }
    public string pAssemblyInfo { get; set; }
    public string pReleaseFolder { get; set; }
    public string pAssemblyName { get; set; }
    public string pIconName { get; set; }
    public string pFullPath { get; set; }
    public string pName { get; set; }
    public string pType { get; set; }
    public int pReleaseMethod { get; set; }
    public string pReleaseUrl { get; set; }
    public string pUsername { get; set; }
    public string pPassword { get; set; }
}
