﻿using System.Collections.Generic;
using System;

public class ExcludeList
{
    private List<string> exFiles;
    private List<string> exFolders;

    public List<string> ExcludeFiles
    {
        get
        {
            return exFiles ?? (exFiles = new List<string>());
        }
        set { exFiles = value; }
    }

    public List<string> ExcludeFolders
    {
        get
        {
            return exFolders ?? (exFolders = new List<string>());
        }
        set { exFolders = value; }
    }

    public ExcludeList()
    {
        exFiles = new List<string>();
        exFolders = new List<string>();
    }
}
