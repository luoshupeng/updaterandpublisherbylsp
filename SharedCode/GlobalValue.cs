﻿using System.Reflection;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

public class GlobalValue
{
    public static string HistoryListName
    {
        get { return "HistoryList.xml"; }
    }
    public static string ReleaseListName
    {
        get { return "ReleaseList.xml"; }
    }
    public static string ProjectInfoName
    {
        get { return "ProjectInfo.xml"; }
    }
    public static string ExcludeListName
    {
        get { return "ExcludeList.xml"; }
    }

    public static string LogDataPath
    {       
        get 
        {
            string dir = Assembly.GetExecutingAssembly().Location;
            dir = dir.Substring(0,dir.LastIndexOf("\\")) + @"\LogData\";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return  dir;
        }
    }

    public static string ProjectDataPath
    {
        get
        {
            string dir = Assembly.GetExecutingAssembly().Location;
            dir = dir.Substring(0,dir.LastIndexOf("\\")) + @"\ProjectData\";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return dir;
        }
    }

    public static int CompareVersion(string firstVer, string secondVer)
    {
        if (string.IsNullOrEmpty(firstVer) || string.IsNullOrEmpty(secondVer))
            return 1;
        var myVersion = firstVer.Split('.');
        var otherVersion = secondVer.Split('.');
        var size = Math.Max(myVersion.Length, otherVersion.Length);
        for (int i = 0; i < size; i++)
        {
            int firstNum = 0;
            if (myVersion.Length > i)
                firstNum = int.Parse(myVersion[i]);
            int secondNum = 0;
            if (otherVersion.Length > i)
                secondNum = int.Parse(otherVersion[i]);
            if (firstNum != secondNum)
                return firstNum - secondNum;
        }
        return 0;
    }

    public static string MD5(string input)
    {
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] inBytes = Encoding.Default.GetBytes(input);
        byte[] outBytes = md5.ComputeHash(inBytes);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < outBytes.Length; i++)
        {
            sb.Append(outBytes[i].ToString("X2"));
        }
        return sb.ToString();
    }

    public static string FormatFileSize(long bytes)
    {
        if (bytes > 1024 * 1024)
            return string.Format("{0}M", Math.Round((double)bytes / (1024 * 1024), 2, MidpointRounding.AwayFromZero));
        if (bytes > 1024)
            return string.Format("{0}K", Math.Round((double)bytes / 1024, 2, MidpointRounding.AwayFromZero));
        return string.Format("{0}B", bytes);
    }

    public static void StartExec(string execName, string execParam = "ok")
    {
        try
        {
            Process.Start(execName, execParam);
        }
        catch (System.Exception ex)
        {
            MessageBox.Show(ex.Message);
        }
    }
}
